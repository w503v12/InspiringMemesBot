FROM python:2.7

RUN mkdir -p /etc/inspiringmemes/
RUN mkdir -p /etc/inspiringmemes/memehof

WORKDIR /etc/inspiringmemes/

COPY requirements.txt ./
RUN pip install certifi chardet DateTime future idna logging python-telegram-bot pytz requests urllib3 zope.interface

COPY ./memehof /etc/inspiringmemes/memehof/
COPY ./InspiringMemes.py /etc/inspiringmemes/

CMD [ "python", "/etc/inspiringmemes/InspiringMemes.py" ]